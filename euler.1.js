/*
  https://projecteuler.net/problem=1
  http://jsbin.com/purasup/edit?html,js,console,output
*/
console.time('Execution');
!(function(){
  var _sumOf3 = _sumOf5 = _sumOf15 = 0;
  for (var i = 1; i < 1000/3; i++) {
    var _j=i*3;
    if(_j%15 !== 0){
      _sumOf3 += _j;
    }
  }
  for (var i = 1; i < 1000/5; i++) {
    var _j=i*5;
    if(_j%15 !== 0){
      _sumOf5 += _j;
    }
  }
  for (var i = 1; i < 1000/15; i++) {
    var _j=i*15;
    _sumOf15 += _j;
  }
  console.log('The sum of all the multiples of 3 or 5 below 1000 is :', (_sumOf3+_sumOf5+_sumOf15));
})();
console.timeEnd('Execution');
